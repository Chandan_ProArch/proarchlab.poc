# Directory level CI pipeline configuration

## Steps

*  Create a project with 'README.md' file so that the repository can be cloned
*  Add two folders - App1 & App2
*  Clone the repository to your local
*  Create a console application in each of the folders with its respective '.sln' files
*  Commit and push the code
*  Add '.gitlab-ci.yml' file to the root of the repository
*  Define a stage for each app with path pointing to its respective '.sln' file in '.gitlab-ci.yml' file
*  Add 'only' keyword to each stage defined with its respective path, so that only that app is built on CI pipeline trigger